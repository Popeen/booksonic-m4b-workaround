$dbScript = ".\booksonic.script"
$isAdmin = [bool](([System.Security.Principal.WindowsIdentity]::GetCurrent()).groups -match "S-1-5-32-544")


$shouldRun = $true
$shouldStart = $false

if(!(Test-Path "$dbScript")){
    if(!(Test-Path c:\booksonic\db\booksonic.script)){
        Write-Warning "Could not find your Booksonic db data. Please put the script in the db directory and run it again."
        Pause
        exit
    }else{
        cd c:\booksonic\db\
    }
}

$service = Get-Service Subsonic

if($service.Status -eq "Running"){
    Write-Host "The Booksonic service is running"
    if($isAdmin){
        Write-Host "Stopping the service"
        Stop-Service Subsonic
        $shouldStart = $true
    }else{
        Write-Warning "You need to stop the Booksonic service before running the script, alternativly you can run the script as administrator and it will do it for you."
        $shouldRun = $false;
    }
}

if($shouldRun){
    Start-Sleep -Seconds 10 #Waiting for Service to stop and save the new dbscript file before we modify it
    Write-Host "Modifying the database script file"
    Set-Content -Path $dbScript -Value (Get-Content $dbScript).replace("ffmpeg -i %s -map 0:0 -b:a %bk -v 0 -f mp3 -", "ffmpeg -i %s -acodec libmp3lame -b:a %bk -f mp3 -")
    Set-Content -Path $dbScript -Value (get-content -Path $dbScript | Select-String -Pattern 'PLAYER_TRANSCODING2' -NotMatch)
}

if($shouldStart -and $shouldRun){
    Write-Host "Starting the Booksonic service"
    Start-Service Subsonic
}

Pause